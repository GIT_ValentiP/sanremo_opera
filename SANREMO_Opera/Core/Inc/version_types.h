/*
 ******************************************************************************
 *  @file      : types.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 19 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_VERSION_TYPES_H_
#define INC_VERSION_TYPES_H_


/* Includes ----------------------------------------------------------*/

/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/
#define OPERA_FW_VERSION          0x00
#define OPERA_FW_SUBVERSION       0x25


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/



#endif /* INC_VERSION_TYPES_H_ */
