/*
 ******************************************************************************
 *  @file      : ringbuffer.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 23 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_RINGBUFFER_H_
#define INC_RINGBUFFER_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
/* Typedef -----------------------------------------------------------*/
// types
typedef struct ring_buffer_s
{
    unsigned char * data;   // pointer to external buffer
    int size;               // size of external buffer
    int head;               // index of free space in external buffer
    int tail;               // index of last item to be read
    int count;              // hold number of items stored but not read
} ring_buffer_t;

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/


// interface
void RING_Init(ring_buffer_t * const buff_state, unsigned char * in_buffer, int buffer_size_in_bytes);
void RING_Reset(ring_buffer_t * const buff_state);      // note: does not clear external buffer
int  RING_IsEmpty(ring_buffer_t * const buff_state);    // return non-zero if buffer is empty
int  RING_IsFull(ring_buffer_t * const buff_state);     // return non-zero if buffer is full
int  RING_AddItem(ring_buffer_t * const buff_state, unsigned char new_item);            // return non-zero if buffer is full
int  RING_GetItem(ring_buffer_t * const buff_state, unsigned char * const out_item);    // return non-zero if buffer is empty


#endif /* INC_RINGBUFFER_H_ */
