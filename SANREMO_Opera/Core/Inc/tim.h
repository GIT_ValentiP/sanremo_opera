/**
  ******************************************************************************
  * File Name          : TIM.h
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __tim_H
#define __tim_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
extern TIM_HandleTypeDef htim14;
extern TIM_HandleTypeDef htim16;
extern TIM_HandleTypeDef htim17;

/* USER CODE BEGIN Private defines */
#define TIMER_PRESCALER_TO_1MHZ                    48
#define TIMER_UP_RELOAD_TO_100us           99//(65536-100)
#define TIMER_UP_RELOAD_TO_1ms            999//(65536-1000)
#define TIMER_UP_RELOAD_TO_10ms          9999//(65536 -10000)


#define TIME_BASE_TIM6                    100 //microseconds
#define TICK_TO_10MS                (10000 / TIME_BASE_TIM6)
#define TICK_TO_1S                  (TICK_TO_10MS *100)
/* USER CODE END Private defines */

void MX_TIM6_Init(void);
void MX_TIM7_Init(void);
void MX_TIM14_Init(void);
void MX_TIM16_Init(void);
void MX_TIM17_Init(void);

/* USER CODE BEGIN Prototypes */
void Main_Clock_Init(void);
void System_Clock_Timer_TickTIM6(void);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ tim_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
