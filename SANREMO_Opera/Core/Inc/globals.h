/*
 * globals.h
 *
 *  Created on: 07 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_


#include "main.h"

/*MODBUS HOLDING REGISTER ADDRESS*/
#define MDB_ENCODER_REG                                       0x203
#define MDB_FW_REL_REG                                        0x100
#define MDB_KEYBOARD_BITMAP_REG                               0x200
#define MDB_KEYBOARD_FUNCTION_REG                             0x202
#define MDB_OPERA_STATUS_REG                                  0x201
#define MDB_LED_DOSE1_RED_REG                                 0x300
#define MDB_LED_DOSE1_GREEN_REG                               0x301
#define MDB_LED_DOSE1_BLUE_REG                                0x302
#define MDB_LED_DOSE2_RED_REG                                 0x303
#define MDB_LED_DOSE2_GREEN_REG                               0x304
#define MDB_LED_DOSE2_BLUE_REG                                0x305
#define MDB_LED_DOSE3_RED_REG                                 0x306
#define MDB_LED_DOSE3_GREEN_REG                               0x307
#define MDB_LED_DOSE3_BLUE_REG                                0x308
#define MDB_LED_DOSE_MAN_RED_REG                              0x309
#define MDB_LED_DOSE_MAN_GREEN_REG                            0x30A
#define MDB_LED_DOSE_MAN_BLUE_REG                             0x30B
#define MDB_DISPLAY16SEG_REG                                  0x30C
#define MDB_LED5_RED_REG                                      0x400
#define MDB_LED5_GREEN_REG                                    0x401
#define MDB_LED5_BLUE_REG                                     0x402
#define MDB_LED6_WHITE_REG                                    0x403

#define MDB_KEYBOARD_CONFIG_REG                              0x1000
#define MDB_TIME_LONG_PRESS_REG                              0x1001
#define MDB_TIME_INTERCLICK_PRESS_REG                        0x1002



#define TIMEOUT_100ms_NO_MSG_MDB                  5//in 100ms




#define NUM_LED_RGB 6
#define SW1_LED_RGB 0
#define SW2_LED_RGB 1
#define SW3_LED_RGB 2
#define SW4_LED_RGB 3
#define SW5_LED_RGB 4
#define SW6_LED_RGB 5

#define LEN_RX_RS232 6
#define LEN_TX_RS232 6
#define RGB_DELTA_TRANSITION 615//15% duty //205 5% duty



#define NO_KEY_PRESSED          0u
#define LEFT_SINGLE_KEY_PRESSED 1u
#define RIGHT_DOUBLE_PRESSED    2u
#define KEY_LONG_PRESSED        3u
/*MODBUS Registers*/
/* ----------------------- Static variables ---------------------------------*/

uint8_t buffer_RX_RS232[10];
uint8_t buffer_TX_RS232[10];

unsigned char char_rx_to_display,led_to_change,duty_red,duty_green,duty_blue;
RS485_PARAM_READ_enum_t parameter_to_read;
bool new_data_rx;
bool wait_reply;

unsigned int timeout_mdb_no_msg;

unsigned char TOGGLE_PIN_LED;
ENCODER_TYPE_t ENCODER_default;//={.Absolute=0,.Diff=0,.Rotation=Enc_Rotate_Nothing,.Mode=Encoder_Mode_0,.LastA=0,.Enc_Count=0};
ENCODER_TYPE_t* encoder_data;//=&(ENCODER_TYPE_t){.Absolute=0,.Diff=0,.Rotation=Enc_Rotate_Nothing,.Mode=Encoder_Mode_0,.LastA=0,.Enc_Count=0};

unsigned char read_keyboard_event;
unsigned char display_refresh_event;
unsigned char display_refresh_start_event;
unsigned char PCA9685_delay_event;
/*Clock System timer Variables*/
sys_timer_t main_clock;

unsigned char char_disp_17seg;
RGB_LED_t led_RGB[NUM_LED_RGB];
unsigned int  RGB_color_start[3];
unsigned char RGB_Transition;

unsigned int short_press_time;
unsigned int long_press_time;
unsigned int click_double_press_time;

Keys_Func_t keyboard_raw;
Keys_Func_t keyboard_short;
Keys_Func_t keyboard_long;

Keys_Func_t keyboard_1click;
Keys_Func_t keyboard_2click;

Keys_Func_Opera_t keyboard_dose;

uint32_t version_fw;

uint16_t interclik_time_ms;
uint16_t longpress_time_ms;
uint16_t adr_modbus_reg;
OPERA_STATUS_enum_t opera_state;
OPERA_CONFIG_enum_t opera_keyboard_conf;

uint8_t  DP_SW_RS485_adr;

uint32_t last_recv_mdb_time ;
#endif /* INC_GLOBALS_H_ */
