/*
 ******************************************************************************
 *  @file      : uart_dma_process.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 23 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_UART_DMA_PROCESS_H_
#define INC_UART_DMA_PROCESS_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "ringbuffer.h"


/* Typedef -----------------------------------------------------------*/

#define DIM_RS485_FRAME           5

/**
 * @brief  Number to display
 */
typedef enum {
 START_BYTE_ADR_STATE,
 FUNC_CODE_STATE,
 DATA_FIRST_STATE,
 DATA_SUCC_STATE,
 CRC_LOW_STATE,
 CRC_HIGH_STATE,
 ERR_STATE,
 FRAME_OK_STATE
} RS485_State_t;

typedef struct rs485_buffer_s
{
    uint8_t      *data;//[DIM_RS485_FRAME];   // pointer to external buffer
    uint16_t      size;               // size of external buffer
    uint16_t      last_pos;
    uint16_t      len_data;
    RS485_State_t state;
    uint16_t      crc_rx;
    uint16_t      crc_check;
    uint8_t       command;
    uint8_t       cmd_type;
    uint8_t       frame_check; //1=ready : frame OK; 0:busy :frame parsing /error
    byte_bit_t    data_body[4];
} rs485_buffer_t;

/* Define ------------------------------------------------------------*/
/**
 * \brief           Calculate length of statically allocated array
 */
#define ARRAY_LEN(x)            (sizeof(x) / sizeof((x)[0]))


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/

/**
 * \brief           Buffer for USART DMA
 * \note            Contains RAW unprocessed data received by UART and transfered by DMA
 */
//static uint8_t usart_rx_dma_buffer[64];

/**
 * \brief           Create ring buffer for TX DMA
 */
//static ringbuff_t usart_tx_dma_ringbuff;

/**
 * \brief           Ring buffer data array for TX DMA
 */
//static uint8_t  usart_tx_dma_ringbuff_data[128];

/**
 * \brief           Length of TX DMA transfer
 */
//static size_t usart_tx_dma_current_len;


/* Function prototypes -----------------------------------------------*/
/* USART related functions */
void usart_RS485_init(void);
void usart_RS485_rx_check(void);
void usart_RS485_process_data(const void* data, size_t len);
void usart_RS485_send_buffer(void);//void usart_RS485_send_buffer(unsigned char* buffer ,unsigned int len);


#endif /* INC_UART_DMA_PROCESS_H_ */
