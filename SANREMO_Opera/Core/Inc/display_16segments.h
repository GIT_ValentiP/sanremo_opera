/*
 ******************************************************************************
 *  @file      : display_16segments.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 22 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_DISPLAY_16SEGMENTS_H_
#define INC_DISPLAY_16SEGMENTS_H_


/* Includes ----------------------------------------------------------*/
#include "stdint.h"
#include "main.h"
/* Typedef -----------------------------------------------------------*/
/**
 * @brief  Number to display
 */
typedef enum {
	ZERO, /*!<  */
	ONE, /*!<  */
	TWO, /*!<  */
	THREE, /*!<  */
	FOUR, /*!<  */
	FIVE, /*!<  */
	SIX, /*!<  */
	SEVEN, /*!<  */
	HEIGTH, /*!<  */
	NINE, /*!<  */
	TEN, /*!<  */
	ELEVEN, /*!<  */
	TWELVE, /*!<  */
	THIRTEEN, /*!<  */
	FOURTEEN, /*!<  */
	FIVETEEN,
} DISPLAY_16SEG_Num_t;

/**
 * @brief  Number to display
 */
typedef enum {
ON_DISP,
OFF_DISP,
BLINK_DISPL
} LED_STATUS_t;


/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/

/* Function prototypes -----------------------------------------------*/
void Display16Seg_All_Single( unsigned char seg_n,LED_STATUS_t status);
void Display16Seg_Start(void);

/**
 * @brief  Display Char on Display 16 segments
 * @note
 * @param  charToDisplay: ascii code of char to display ;Dot : 1 if dot present 0, if not present;
 * @retval None
 */
void Display16Seg_Char( unsigned char chrToDisp ,unsigned char dot,LED_STATUS_t status);

/**
 * @brief  Display Number from 0 to F on Display 16 segments
 * @note
 * @param  numrToDisp: number to display ;Dot : 1 if dot present 0, if not present;,LED_STATUS_t status :status of animation on display
 * @retval None
 */
void Display16Seg_Num( unsigned char numToDisp ,unsigned char dot,LED_STATUS_t status);


#endif /* INC_DISPLAY_16SEGMENTS_H_ */
