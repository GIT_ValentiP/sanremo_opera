/*
 ******************************************************************************
 *  @file      : display_16segments.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 22 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "display_16segments.h"
#include "globals.h"
#include "iwdg.h"
/* Private includes ----------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/*
 * This repository contains integer literals for displaying ASCII characters on 7 segment, 14 segment, and 16 segment LED displays.
 * I needed a set of 16 segment and 7 segment ASCII characters for a project, but I couldn't find a readily-available library. I'm sharing my own character patterns so others won't have this problem.
 *
 * Each character set covers all visible ASCII characters, starting at 32 ('space') and ending at 127 ('del'). Segments are ordered sequentially.
 *
 *For each set the segments are ordered sequentially. Bit '0' corresponds to segment 'A', bit '1' corresponds to segment 'B', and so-on. Here are the ordered segments for each display:

    16 Segment: DP-U-T-S-R-P-N-M-K-H-G-F-E-D-C-B-A  //For Display WCN1X-0480WW-A22-F  :  16 Segment: DP-G1-N-M-L-G2-K-J-H-F-E-D2-D1-C-B-A2-A1
    14 Segment: DP-N-M-L-K-J-H-G2-G1-F-E-D-C-B-A
    7 Segment: DP-G-F-E-D-C-B-A
 *
 */
const uint16_t SixteenSegmentASCII[96] = {
	0b0000000000000000, /* (space) */
	0b0000000000001100, /* ! */
	0b0000001000000100, /* " */
	0b1010101000111100, /* # */
	0b1010101010111011, /* $ */
	0b1110111010011001, /* % */
	0b1001001101110001, /* & */
	0b0000001000000000, /* ' */
	0b0001010000000000, /* ( */
	0b0100000100000000, /* ) */
	0b1111111100000000, /* * */
	0b1010101000000000, /* + */
	0b0100000000000000, /* , */
	0b1000100000000000, /* - */
	0b0001000000000000, /* . */
	0b0100010000000000, /* / */
	0b0100010011111111, /* 0 */
	0b0000010000001100, /* 1 */
	0b1000100001110111, /* 2 */
	0b0000100000111111, /* 3 */
	0b1000100010001100, /* 4 */
	0b1001000010110011, /* 5 */
	0b1000100011111011, /* 6 */
	0b0000000000001111, /* 7 */
	0b1000100011111111, /* 8 */
	0b1000100010111111, /* 9 */
	0b0010001000000000, /* : */
	0b0100001000000000, /* ; */
	0b1001010000000000, /* < */
	0b1000100000110000, /* = */
	0b0100100100000000, /* > */
	0b0010100000000111, /* ? */
	0b0000101011110111, /* @ */
	0b1000100011001111, /* A */
	0b0010101000111111, /* B */
	0b0000000011110011, /* C */
	0b0010001000111111, /* D */
	0b1000000011110011, /* E */
	0b1000000011000011, /* F */
	0b0000100011111011, /* G */
	0b1000100011001100, /* H */
	0b0010001000110011, /* I */
	0b0000000001111100, /* J */
	0b1001010011000000, /* K */
	0b0000000011110000, /* L */
	0b0000010111001100, /* M */
	0b0001000111001100, /* N */
	0b0000000011111111, /* O */
	0b1000100011000111, /* P */
	0b0001000011111111, /* Q */
	0b1001100011000111, /* R */
	0b1000100010111011, /* S */
	0b0010001000000011, /* T */
	0b0000000011111100, /* U */
	0b0100010011000000, /* V */
	0b0101000011001100, /* W */
	0b0101010100000000, /* X */
	0b1000100010111100, /* Y */
	0b0100010000110011, /* Z */
	0b0010001000010010, /* [ */
	0b0001000100000000, /* \ */
	0b0010001000100001, /* ] */
	0b0101000000000000, /* ^ */
	0b0000000000110000, /* _ */
	0b0000000100000000, /* ` */
	0b1010000001110000, /* a */
	0b1010000011100000, /* b */
	0b1000000001100000, /* c */
	0b0010100000011100, /* d */
	0b1100000001100000, /* e */
	0b1010101000000010, /* f */
	0b1010001010100001, /* g */
	0b1010000011000000, /* h */
	0b0010000000000000, /* i */
	0b0010001001100000, /* j */
	0b0011011000000000, /* k */
	0b0000000011000000, /* l */
	0b1010100001001000, /* m */
	0b1010000001000000, /* n */
	0b1010000001100000, /* o */
	0b1000001011000001, /* p */
	0b1010001010000001, /* q */
	0b1000000001000000, /* r */
	0b1010000010100001, /* s */
	0b1000000011100000, /* t */
	0b0010000001100000, /* u */
	0b0100000001000000, /* v */
	0b0101000001001000, /* w */
	0b0101010100000000, /* x */
	0b0000101000011100, /* y */
	0b1100000000100000, /* z */
	0b1010001000010010, /* { */
	0b0010001000000000, /* | */
	0b0010101000100001, /* } */
	0b1100110000000000, /* ~ */
	0b0000000000000000, /* (del) */
};

const uint16_t SixteenSegmentNUM[16] = {
	0b0100010011111111, /* 0 */
	0b0000010000001100, /* 1 */
	0b1000100001110111, /* 2 */
	0b0000100000111111, /* 3 */
	0b1000100010001100, /* 4 */
	0b1001000010110011, /* 5 */
	0b1000100011111011, /* 6 */
	0b0000000000001111, /* 7 */
	0b1000100011111111, /* 8 */
	0b1000100010111111, /* 9 */
	0b1000100011001111, /* A */
	0b0010101000111111, /* B */
	0b0000000011110011, /* C */
	0b0010001000111111, /* D */
	0b1000000011110011, /* E */
	0b1000000011000011, /* F */
};
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

/* Function prototypes -----------------------------------------------*/
/**
 * @brief  Display Char on Display 16 segments
 * @note
 * @param  charToDisplay: ascii code of char to display ;Dot : 1 if dot present 0, if not present;
 * @retval None
 */
void Display16Seg_All_Single( unsigned char seg_n,LED_STATUS_t status){
GPIO_PinState led_status;
if (status==ON_DISP){
  	led_status= GPIO_PIN_SET;

  }else if (status==OFF_DISP){
  	led_status= GPIO_PIN_RESET;
  }else if (TOGGLE_PIN_LED==1){
  	led_status= GPIO_PIN_SET;
  }else{
  	led_status= GPIO_PIN_RESET;
  }

 if (seg_n <16) {
          switch(seg_n)   {
				case(0):
						HAL_GPIO_WritePin(SEG_A1_GPIO_Port,SEG_A1_Pin,led_status); //
						break;
				case(1):
					    HAL_GPIO_WritePin(SEG_A2_GPIO_Port,SEG_A2_Pin,led_status); //
						break;
				case(2):
						HAL_GPIO_WritePin(SEG_B_GPIO_Port,SEG_B_Pin,led_status); //
						break;
				case(3):
						HAL_GPIO_WritePin(SEG_C_GPIO_Port,SEG_C_Pin,led_status); //
						break;
				case(4):
		                HAL_GPIO_WritePin(SEG_D2_GPIO_Port,SEG_D2_Pin,led_status); //
						break;
				case(5):
			            HAL_GPIO_WritePin(SEG_D1_GPIO_Port,SEG_D1_Pin,led_status); //
						break;
				case(6):
						HAL_GPIO_WritePin(SEG_E_GPIO_Port,SEG_E_Pin,led_status); //
						break;
				case(7):
						HAL_GPIO_WritePin(SEG_F_GPIO_Port,SEG_F_Pin,led_status); //
						break;
				case(8):
						HAL_GPIO_WritePin(SEG_H_GPIO_Port,SEG_H_Pin,led_status); //
						break;
				case(9):
						HAL_GPIO_WritePin(SEG_J_GPIO_Port,SEG_J_Pin,led_status); //
						break;
				case(10):
						HAL_GPIO_WritePin(SEG_K_GPIO_Port,SEG_K_Pin,led_status); //
						break;
				case(11):
						HAL_GPIO_WritePin(SEG_G2_GPIO_Port,SEG_G2_Pin,led_status); //
						break;
				case(12):
						HAL_GPIO_WritePin(SEG_L_GPIO_Port,SEG_L_Pin,led_status); //
						break;
				case(13):
						HAL_GPIO_WritePin(SEG_M_GPIO_Port,SEG_M_Pin,led_status); //
						break;
				case(14):
						HAL_GPIO_WritePin(SEG_N_GPIO_Port,SEG_N_Pin,led_status); //
						break;
				case(15):
						HAL_GPIO_WritePin(SEG_G1_GPIO_Port,SEG_G1_Pin,led_status); //
						break;

				}
 }else{//ALL segment
		        HAL_GPIO_WritePin(SEG_A1_GPIO_Port,SEG_A1_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_A2_GPIO_Port,SEG_A2_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_B_GPIO_Port,SEG_B_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_C_GPIO_Port,SEG_C_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_D1_GPIO_Port,SEG_D1_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_D2_GPIO_Port,SEG_D2_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_E_GPIO_Port,SEG_E_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_F_GPIO_Port,SEG_F_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_G1_GPIO_Port,SEG_G1_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_G2_GPIO_Port,SEG_G2_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_H_GPIO_Port,SEG_H_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_J_GPIO_Port,SEG_J_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_K_GPIO_Port,SEG_K_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_L_GPIO_Port,SEG_L_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_M_GPIO_Port,SEG_M_Pin,led_status); //

				HAL_GPIO_WritePin(SEG_N_GPIO_Port,SEG_N_Pin,led_status); //
 }

}



/**
 * @brief  Start animation on Display 16 segments
 * @note
 * @param  None
 * @retval None
 */
void Display16Seg_Start(void){

uint8_t id;
id=16;//all segments
Display16Seg_All_Single(id,OFF_DISP);
display_refresh_start_event=0;
for(id=0;id<16;id++){
	Display16Seg_All_Single(id,ON_DISP);
	display_refresh_start_event=0;
	while(display_refresh_start_event==0){
	}
	HAL_IWDG_Refresh(&hiwdg);
}
Display16Seg_All_Single(id,OFF_DISP);
HAL_IWDG_Refresh(&hiwdg);
}








/**
 * @brief  Display Char on Display 16 segments
 * @note
 * @param  charToDisplay: ascii code of char to display ;Dot : 1 if dot present 0, if not present;
 * @retval None
 */
void Display16Seg_Char( unsigned char chrToDisp ,unsigned char dot,LED_STATUS_t status){
uint16_t chr_bitmap;
uint16_t mask_bit=0x0001;
uint8_t id;
GPIO_PinState led_status;

    if ((chrToDisp < 32) || (chrToDisp > 127)){
    	chr_bitmap= SixteenSegmentASCII[0];

    }else{
    	id=chrToDisp -32;
    	chr_bitmap= SixteenSegmentASCII[id];
    }

    if (status==ON_DISP){
    	led_status= GPIO_PIN_SET;

    }else if (status==OFF_DISP){
    	led_status= GPIO_PIN_RESET;
    }else if (TOGGLE_PIN_LED==1){
    	led_status= GPIO_PIN_SET;
    }else{
    	led_status= GPIO_PIN_RESET;
    }

    if (led_status== GPIO_PIN_SET){
    	mask_bit=0x0001;
    	for(id=0;id<16;id++){
    		mask_bit=0x0001;
			mask_bit <<=id;
			if ((chr_bitmap & mask_bit)!=0){
				Display16Seg_All_Single( id,ON_DISP);
			}else{
				Display16Seg_All_Single( id,OFF_DISP);
			}
    	}
    }else{
    	Display16Seg_All_Single( 16,OFF_DISP);

    }
    if (dot==1){
    	HAL_GPIO_WritePin(SEG_DP_GPIO_Port,SEG_DP_Pin,led_status); //
    }else{
    	HAL_GPIO_WritePin(SEG_DP_GPIO_Port,SEG_DP_Pin,GPIO_PIN_RESET); //
    }
}

/**
 * @brief  Display Number from 0 to F on Display 16 segments
 * @note
 * @param  numrToDisp: ascii code of char to display ;Dot : 1 if dot present 0, if not present;,LED_STATUS_t status :status of animation on display
 * @retval None
 */
void Display16Seg_Num( unsigned char numToDisp ,unsigned char dot,LED_STATUS_t status){
	uint16_t chr_bitmap;
	uint16_t mask_bit=0x0001;
	uint8_t id;
	GPIO_PinState led_status;

	    if (numToDisp > 15){
	    	id = (numToDisp % 16);
	    	chr_bitmap= SixteenSegmentNUM[id];

	    }else{

	    	chr_bitmap= SixteenSegmentNUM[numToDisp];
	    }

	    if (status==ON_DISP){
	    	led_status= GPIO_PIN_SET;

	    }else if (status==OFF_DISP){
	    	led_status= GPIO_PIN_RESET;
	    }else if (TOGGLE_PIN_LED==1){
	    	led_status= GPIO_PIN_SET;
	    }else{
	    	led_status= GPIO_PIN_RESET;
	    }

	    if (led_status== GPIO_PIN_SET){

	    	for(id=0;id<16;id++){
	    		mask_bit=0x0001;
				mask_bit <<=id;
				if ((chr_bitmap & mask_bit)!=0){

					Display16Seg_All_Single( id,ON_DISP);
				}else{

					Display16Seg_All_Single( id,OFF_DISP);
				}
	    	}
	    }else{

	    	Display16Seg_All_Single( 16,OFF_DISP);
	    }
	    if (dot==1){
	    	HAL_GPIO_WritePin(SEG_DP_GPIO_Port,SEG_DP_Pin,led_status); //
	    }else{
	    	HAL_GPIO_WritePin(SEG_DP_GPIO_Port,SEG_DP_Pin,GPIO_PIN_RESET); //
	    }
}

