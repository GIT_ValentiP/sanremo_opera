/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/

#include <version_types.h>
#include "main.h"
#include "crc.h"
#include "dma.h"
#include "fatfs.h"
#include "i2c.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "display_16segments.h"
#include "version_types.h"
#include "uart_dma_process.h"
#include "globals.h"
#include "mdb_com.h"
/*Modbus Library */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
__attribute__((section(".projectvars"))) const uint32_t  VERSION_NUMBER = ((OPERA_FW_VERSION<<16)| OPERA_FW_SUBVERSION);
__attribute__((section(".projectvars"))) const uint32_t  CRC_PROGRAM= 0x000000000;
__attribute__((section(".projectvars"))) const uint16_t  BUILD_ID=0xA5A5;
__attribute__((section(".projectvars"))) const  uint8_t  OTHER_VAR=0x00;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
unsigned char new_key_pressed,enc_rotation_read;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
void delay_ms(unsigned long millisec);
void app_main_lab(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  //ENCODER_Rotate_t enc_rotation_read;
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */


  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_IWDG_Init();
 // MX_FATFS_Init();
  MX_CRC_Init();
  MX_I2C1_Init();
 // MX_SPI2_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM14_Init();
 // MX_TIM16_Init();
 // MX_TIM17_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
//  MX_USB_DEVICE_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  //HAL_NVIC_DisableIRQ(TIM1_BRK_UP_TRG_COM_IRQn);//Disable TIM1 interrupt tick
  version_fw=VERSION_NUMBER;

  Encoder_Init();

  Main_Clock_Init();

  HAL_TIM_Base_MspInit(&htim14);
  //__HAL_TIM_DISABLE_IT(&htim14,TIM_IT_CC1);//Disable TIM14 input capture interrupt
  HAL_I2C_MspInit(&hi2c1);

  DP_SW_RS485_adr=OPERA_RS485_ADDR;//address  to identify OPERA Keyboard on ModBus
  HAL_IWDG_Refresh(&hiwdg);
  //eMBInit(MB_RTU, DP_SW_RS485_adr, 1, RS485_BAUDRATE, MB_PAR_NONE);/* Modbus Initialization parameter Mode -MB_RTU-, Slave Address,Num COM , BaudRate ,Parity*/
  //eMBEnable();
  RS485_TX_DISABLE;
  RS485_RX_ENABLE;
  printf("\n\r *** OPERA FW Version: %3d.%3d ***\n\r",OPERA_FW_VERSION,OPERA_FW_SUBVERSION);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  /*TURN ON ALL SEGMENTS*/
  HAL_IWDG_Refresh(&hiwdg);
  Display16Seg_Start();

//  char_disp_17seg=32;
//  Display16Seg_Char( char_disp_17seg ,0,ON_DISP);
//  new_key_pressed=17;
//  enc_rotation_read=17;
//  char_rx_to_display=0xFF;
  HAL_IWDG_Refresh(&hiwdg);
  PCA9685_Init_Default(); //LED driver PCA9685 init

  new_data_rx=false;//flag for new data from RS485/RS232

  opera_keyboard_conf=OPERA_NOT_SET;
  opera_state=OPERA_NOT_CONFIG;

  RS485_TX_DISABLE;
  RS485_RX_ENABLE;

  init_input_reayboard();

  HAL_IWDG_Refresh(&hiwdg);
  mdb_comm_Initialize(); //Modbus Communication Init
  timeout_mdb_no_msg=100;//TIMEOUT_100ms_NO_MSG_MDB*2;
  //HAL_UART_Receive_DMA(&huart1, (uint8_t *)buffer_RX_RS232, LEN_RX_RS232);
  //HAL_UART_Receive_DMA(&huart2, (uint8_t *)buffer_RX_RS232, LEN_RX_RS232);//RS485
  while (1)
  {
	HAL_IWDG_Refresh(&hiwdg);
    /* USER CODE END WHILE */
    if (read_keyboard_event==1){
    	 read_input_keyboard();
    	 Encoder_Get(encoder_data);
    	 read_keyboard_event=0;
     }

    if (opera_keyboard_conf==OPERA_4_TASTI){
      Opera_keyboard_function_4Tasti();
    }else if(opera_keyboard_conf==OPERA_7_TASTI){
	  Opera_keyboard_function_7Tasti();
    }

    mdb_comm_processMessage();/*Modbus poll update in each run*/
    if (new_data_rx==true){//received new duty for led
   	 PCA9685_setDutyLed(SW1_LED_RGB*3, led_RGB[SW1_LED_RGB].duty_RED,led_RGB[SW1_LED_RGB].duty_GREEN,led_RGB[SW1_LED_RGB].duty_BLUE);
     PCA9685_setDutyLed(SW2_LED_RGB*3, led_RGB[SW2_LED_RGB].duty_RED,led_RGB[SW2_LED_RGB].duty_GREEN,led_RGB[SW2_LED_RGB].duty_BLUE);
   	 PCA9685_setDutyLed(SW3_LED_RGB*3, led_RGB[SW3_LED_RGB].duty_RED,led_RGB[SW3_LED_RGB].duty_GREEN,led_RGB[SW3_LED_RGB].duty_BLUE);
   	 PCA9685_setDutyLed(SW4_LED_RGB*3, led_RGB[SW4_LED_RGB].duty_RED,led_RGB[SW4_LED_RGB].duty_GREEN,led_RGB[SW4_LED_RGB].duty_BLUE);
     PCA9685_setDutyLed(SW5_LED_RGB*3, led_RGB[SW5_LED_RGB].duty_RED,led_RGB[SW5_LED_RGB].duty_GREEN,led_RGB[SW5_LED_RGB].duty_BLUE);
   	 PCA9685_setDutyLed(SW6_LED_RGB*3, led_RGB[SW6_LED_RGB].duty_RED,led_RGB[SW6_LED_RGB].duty_GREEN,led_RGB[SW6_LED_RGB].duty_BLUE);
   	 Display16Seg_Char(char_rx_to_display,0,ON_DISP);

   	 new_data_rx=false;
     }


    /* USER CODE BEGIN 3 */
    HAL_GPIO_WritePin(LED_SERVICE5_GPIO_Port,LED_SERVICE5_Pin , TOGGLE_PIN_LED);

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_SYSCLK;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* EXTI4_15_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
  /* DMA1_Channel4_5_6_7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
  /* TIM6_DAC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
 // HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
  /* TIM7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM7_IRQn, 2, 0);
 // HAL_NVIC_EnableIRQ(TIM7_IRQn);
  /* TIM14_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM14_IRQn, 2, 0);
  HAL_NVIC_EnableIRQ(TIM14_IRQn);
  /* TIM16_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM16_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM16_IRQn);
  /* TIM17_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM17_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM17_IRQn);
  /* I2C1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(I2C1_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(I2C1_IRQn);
  /* USART1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
// HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USB_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(USB_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(USB_IRQn);
}

/* USER CODE BEGIN 4 */
void delay_ms(unsigned long millisec)
{
	unsigned long cnt_to_ms,iter1,iter2;
	cnt_to_ms=6000;

	for(iter2=0;iter2 < millisec;iter2++){
		for(iter1=0;iter1 < cnt_to_ms;iter1++){
				__NOP();
			}
	}

}


/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */

  HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);
 // HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);


  return ch;
}


void app_main_lab(void){


	 if((char_rx_to_display!=0xFF)&&(new_data_rx==true)){
	   	   Display16Seg_Char(char_rx_to_display,0,ON_DISP);
	   	   char_rx_to_display=0xFF;
	   	   new_data_rx=false;
	    }else if (new_data_rx==true){//received new duty for led
	    	led_RGB[led_to_change].duty_RED=duty_red;
	    	led_RGB[led_to_change].duty_GREEN=duty_green;
	    	led_RGB[led_to_change].duty_BLUE=duty_blue;
	    	if (led_to_change==SW5_LED_RGB){
	    		PCA9685_setDutyLed(SW5_LED_RGB*3, led_RGB[SW5_LED_RGB].duty_RED,led_RGB[SW5_LED_RGB].duty_GREEN,led_RGB[SW5_LED_RGB].duty_BLUE);
	    	}
	    	if (led_to_change==SW6_LED_RGB){
	    	    PCA9685_setDutyLed(SW6_LED_RGB*3, led_RGB[SW6_LED_RGB].duty_RED,led_RGB[SW6_LED_RGB].duty_GREEN,led_RGB[SW6_LED_RGB].duty_BLUE);
	    	}
	    	new_data_rx=false;
	    }

	    usart_RS485_send_buffer();
	    if( enc_rotation_read!=encoder_data->Detent){
			enc_rotation_read=encoder_data->Detent;//keyboard_function_manager();
			Display16Seg_Num((unsigned char)encoder_data->Detent,0,ON_DISP);
			//Display16Seg_Char((unsigned char)encoder_data->Absolute+32,0,ON_DISP);
	    }

	     if( new_key_pressed!=keyboard_function_manager()){
	    	new_key_pressed=keyboard_function_manager();
		    //Display16Seg_Num(new_key_pressed,0,ON_DISP);
	    	//Display16Seg_Char(new_key_pressed+32,0,ON_DISP);

	     }

	     if  (new_key_pressed<1){

			 led_RGB[SW1_LED_RGB].id_col=0;
			 led_RGB[SW2_LED_RGB].id_col=0;
			 led_RGB[SW3_LED_RGB].id_col=0;
			 led_RGB[SW4_LED_RGB].id_col=0;

			 PCA9685_setColorLed(SW1_LED_RGB*3, led_RGB[SW1_LED_RGB].id_col);
			 PCA9685_setColorLed(SW2_LED_RGB*3, led_RGB[SW2_LED_RGB].id_col);
			 PCA9685_setColorLed(SW3_LED_RGB*3, led_RGB[SW3_LED_RGB].id_col);
			 PCA9685_setColorLed(SW4_LED_RGB*3, led_RGB[SW4_LED_RGB].id_col);
		}else if (new_key_pressed<4){
			 led_RGB[SW1_LED_RGB].id_col=new_key_pressed;
			 PCA9685_setDutyLed(SW1_LED_RGB*3, led_RGB[SW1_LED_RGB].duty_RED,led_RGB[SW1_LED_RGB].duty_GREEN,led_RGB[SW1_LED_RGB].duty_BLUE);
			 //PCA9685_setColorLed(SW1_LED_RGB*3, led_RGB[SW1_LED_RGB].id_col);
		}else if (new_key_pressed<7){
			 led_RGB[SW2_LED_RGB].id_col=new_key_pressed;
			 PCA9685_setDutyLed(SW2_LED_RGB*3, led_RGB[SW2_LED_RGB].duty_RED,led_RGB[SW2_LED_RGB].duty_GREEN,led_RGB[SW2_LED_RGB].duty_BLUE);
			 //PCA9685_setColorLed(SW2_LED_RGB*3, led_RGB[SW2_LED_RGB].id_col);
		}else  if (new_key_pressed<10){
			 led_RGB[SW3_LED_RGB].id_col=new_key_pressed;
			 PCA9685_setDutyLed(SW3_LED_RGB*3, led_RGB[SW3_LED_RGB].duty_RED,led_RGB[SW3_LED_RGB].duty_GREEN,led_RGB[SW3_LED_RGB].duty_BLUE);
			 //PCA9685_setColorLed(SW3_LED_RGB*3, led_RGB[SW3_LED_RGB].id_col);
		}else  if (new_key_pressed<13){
			 led_RGB[SW4_LED_RGB].id_col=new_key_pressed;
			 PCA9685_setDutyLed(SW4_LED_RGB*3, led_RGB[SW4_LED_RGB].duty_RED,led_RGB[SW4_LED_RGB].duty_GREEN,led_RGB[SW4_LED_RGB].duty_BLUE);
			 //PCA9685_setColorLed(SW4_LED_RGB*3, led_RGB[SW4_LED_RGB].id_col);
		}



	    if (display_refresh_event!=0){
	        //printf("\n\r LED5 RED: %d ,GREEN: %d ,BLUE: %d",led_RGB[SW5_LED_RGB].duty_RED,led_RGB[SW5_LED_RGB].duty_GREEN,led_RGB[SW5_LED_RGB].duty_BLUE);
	    	PCA9685_RGB_Transition(4,0,RGB_DELTA_TRANSITION);
	    	display_refresh_event=0;
	    }


}

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
    if(last_recv_mdb_time==0xFFFFFFFF){
    	last_recv_mdb_time=0;
    }
    last_recv_mdb_time++;
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
