/*
 * mdb_registers.c
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */


/*=== Include files ====================================================================================================================*/


#include "mdb_registers.h"
#include "globals.h"

/*=== Global function definitions (Local sub-system scope) ============================================================*/

int32_t mdb_read_status(uint16_t index)
{
	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_read_coil(uint16_t index)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_write_coil(uint16_t index, uint16_t value)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;

}

int32_t mdb_read_input_register(uint16_t index)
{

	return -MDB_ERR_ILLEGAL_DATAADDRESS;
}


int32_t mdb_read_holding_register(uint16_t index)
{

	int16_t result_rd=0;
	switch ( index ) {
				case MDB_FW_REL_REG          :
													return (uint16_t) version_fw;

				case MDB_KEYBOARD_BITMAP_REG:

													return (uint16_t) keyboard_raw.bitmap;


				case MDB_KEYBOARD_FUNCTION_REG:
					                                result_rd=keyboard_dose.bitmap;
					                                keyboard_dose.bitmap =0x0000;
													return (uint16_t) result_rd;

				case MDB_OPERA_STATUS_REG:
													return (uint16_t) opera_state;

				case MDB_ENCODER_REG:
					                               result_rd=encoder_data->LastDetentRead;
					                                encoder_data->LastDetentRead=0;
													return (uint16_t) (result_rd);

				case MDB_LED_DOSE1_RED_REG:
														   return (uint16_t) led_RGB[SW1_LED_RGB].duty_RED;

				case MDB_LED_DOSE1_GREEN_REG:
														   return (uint16_t) led_RGB[SW1_LED_RGB].duty_GREEN;

				case MDB_LED_DOSE1_BLUE_REG:
														   return (uint16_t) led_RGB[SW1_LED_RGB].duty_BLUE;

				case MDB_LED_DOSE2_RED_REG:
														    return (uint16_t) led_RGB[SW2_LED_RGB].duty_RED;

				case MDB_LED_DOSE2_GREEN_REG:
															return (uint16_t) led_RGB[SW2_LED_RGB].duty_GREEN;

				case MDB_LED_DOSE2_BLUE_REG:
					                                         return (uint16_t) led_RGB[SW2_LED_RGB].duty_BLUE;

				case MDB_LED_DOSE3_RED_REG:
															   return (uint16_t) led_RGB[SW3_LED_RGB].duty_RED;

				case MDB_LED_DOSE3_GREEN_REG:
															   return (uint16_t) led_RGB[SW3_LED_RGB].duty_GREEN;

				case MDB_LED_DOSE3_BLUE_REG:
					                                           return (uint16_t) led_RGB[SW3_LED_RGB].duty_BLUE;

				case MDB_LED_DOSE_MAN_RED_REG:
															   return (uint16_t) led_RGB[SW4_LED_RGB].duty_RED;

				case MDB_LED_DOSE_MAN_GREEN_REG:
															   return (uint16_t) led_RGB[SW4_LED_RGB].duty_GREEN;

				case MDB_LED_DOSE_MAN_BLUE_REG:
					                                           return (uint16_t) led_RGB[SW4_LED_RGB].duty_BLUE;


				case MDB_DISPLAY16SEG_REG:
																  return (uint16_t) char_rx_to_display;
				case MDB_KEYBOARD_CONFIG_REG:
																  return (uint16_t) opera_keyboard_conf;
				case MDB_TIME_LONG_PRESS_REG:
																  return (uint16_t) longpress_time_ms;



				case MDB_TIME_INTERCLICK_PRESS_REG:
																	  return (uint16_t) interclik_time_ms;

				default:
						 return -MDB_ERR_ILLEGAL_DATAADDRESS;

	}


}




int32_t mdb_write_holding_register(uint16_t index, uint16_t value)
{

	uint16_t new_value;
	 new_data_rx=true;
	 new_value=value;
	 if ((index > 0x2FF) && (index < 0x404)){
		 if (value > 100){
			 new_value=100;
		 }
	 }


	switch ( index ) {


		case MDB_LED_DOSE1_RED_REG:
			  led_RGB[SW1_LED_RGB].duty_RED=new_value;
			  return MDB_OK;

		case MDB_LED_DOSE1_GREEN_REG:
			led_RGB[SW1_LED_RGB].duty_GREEN=new_value;
			return MDB_OK;

		case MDB_LED_DOSE1_BLUE_REG:
			led_RGB[SW1_LED_RGB].duty_BLUE=new_value;
			return MDB_OK;

		case MDB_LED_DOSE2_RED_REG:
			led_RGB[SW2_LED_RGB].duty_RED=new_value;
		    return MDB_OK;

		case MDB_LED_DOSE2_GREEN_REG:
			led_RGB[SW2_LED_RGB].duty_GREEN=new_value;
			 return MDB_OK;

		case MDB_LED_DOSE2_BLUE_REG:
			led_RGB[SW2_LED_RGB].duty_BLUE=new_value;
			return MDB_OK;

		case MDB_LED_DOSE3_RED_REG:
			led_RGB[SW3_LED_RGB].duty_RED=new_value;
		    return MDB_OK;

		case MDB_LED_DOSE3_GREEN_REG:
			led_RGB[SW3_LED_RGB].duty_GREEN=new_value;
			return MDB_OK;

		case MDB_LED_DOSE3_BLUE_REG:
			led_RGB[SW3_LED_RGB].duty_BLUE=new_value;
		    return MDB_OK;

		case MDB_LED_DOSE_MAN_RED_REG:
			led_RGB[SW4_LED_RGB].duty_RED=new_value;
	        return MDB_OK;

		case MDB_LED_DOSE_MAN_GREEN_REG:
			led_RGB[SW4_LED_RGB].duty_GREEN=new_value;
		    return MDB_OK;

		case MDB_LED_DOSE_MAN_BLUE_REG:
			led_RGB[SW4_LED_RGB].duty_BLUE=new_value;
			return MDB_OK;


		case MDB_DISPLAY16SEG_REG:
			char_rx_to_display=value;
			return MDB_OK;

		case MDB_LED5_RED_REG:
			led_RGB[SW5_LED_RGB].duty_RED=new_value;
			return MDB_OK;

		case MDB_LED5_GREEN_REG:
			led_RGB[SW5_LED_RGB].duty_GREEN=new_value;
			return MDB_OK;

		case MDB_LED5_BLUE_REG:
			led_RGB[SW5_LED_RGB].duty_BLUE=new_value;
			return MDB_OK;


		case MDB_LED6_WHITE_REG:
			led_RGB[SW6_LED_RGB].duty_RED=led_RGB[SW6_LED_RGB].duty_GREEN =led_RGB[SW6_LED_RGB].duty_BLUE=new_value;
			return MDB_OK;


		case MDB_KEYBOARD_CONFIG_REG:
		    new_data_rx=false;
		    if (value <2){
				opera_keyboard_conf=value;
				if (opera_state==OPERA_NOT_CONFIG){
						 opera_state=OPERA_READY;
				 }
		    }
            return MDB_OK;

		case MDB_TIME_LONG_PRESS_REG:
			 new_data_rx=true;

		     longpress_time_ms=value;
		     init_input_reayboard();
		     new_data_rx=false;
		     return MDB_OK;

		case MDB_TIME_INTERCLICK_PRESS_REG:
		    new_data_rx=true;

			interclik_time_ms=value;
			 init_input_reayboard();
			 new_data_rx=false;
		    return MDB_OK;

		default:
			     new_data_rx=false;
			     return -MDB_ERR_ILLEGAL_DATAADDRESS;

		}


}
