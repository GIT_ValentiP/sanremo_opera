/*
 ******************************************************************************
 *  @file      : ringbuffer.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 23 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "ringbuffer.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

// function definitions
void RING_Init(ring_buffer_t * const buff_state, unsigned char * in_buffer, int buffer_size_in_bytes)
{
//    assert(0 != buff_state);
//    assert(0 != in_buffer);
//    assert(buffer_size_in_bytes != 0);

    buff_state->data = in_buffer;
    buff_state->size = buffer_size_in_bytes;
    buff_state->head = 0;
    buff_state->tail = 0;
    buff_state->count = 0;
}

void RING_Reset(ring_buffer_t * const buff_state)
{
//    assert(0 != buff_state);

    buff_state->head = 0;
    buff_state->tail = 0;
    buff_state->count = 0;
}

// return non-zero value if buffer is empty
int  RING_IsEmpty(ring_buffer_t * const buff_state)
{
    int fBufferIsEmpty = 0;

//    assert(0 != buff_state);

    if (0 == buff_state->count)
    {
        fBufferIsEmpty = 1;
    }

    return fBufferIsEmpty;
}

// return non-zero value if buffer is full
int  RING_IsFull(ring_buffer_t * const buff_state)
{
    int fBufferIsFull = 0;

//    assert(0 != buff_state);

    if (buff_state->count == buff_state->size)
    {
        fBufferIsFull = 1;
    }

    return fBufferIsFull;
}

// return non-zero value if buffer is full
int  RING_AddItem(ring_buffer_t * const buff_state, unsigned char new_item)
{
    int fBufferIsFull = 0;

//    assert(0 != buff_state);

    fBufferIsFull = RING_IsFull(buff_state);

    if (!fBufferIsFull)
    {
        buff_state->data[buff_state->head] = new_item;
        buff_state->head++;
        buff_state->count++;

        if (buff_state->head == buff_state->size)
        {
            buff_state->head = 0;
        }
    }


    return fBufferIsFull;
}

// return non-zero value if buffer is empty
int  RING_GetItem(ring_buffer_t * const buff_state, unsigned char * const out_item)
{
    int fBufferIsEmpty = 0;

//    assert(0 != buff_state);
//    assert(0 != out_item);

    fBufferIsEmpty = RING_IsEmpty(buff_state);

    if (!fBufferIsEmpty)
    {
        *out_item = buff_state->data[buff_state->tail];
        buff_state->tail++;
        buff_state->count--;

        if (buff_state->tail == buff_state->size)
        {
            buff_state->tail = 0;
        }
    }

    return fBufferIsEmpty;
}
