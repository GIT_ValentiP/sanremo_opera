/*
 ******************************************************************************
 *  @file      : uart_dma_process.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 23 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "uart_dma_process.h"
#include "globals.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
rs485_buffer_t RS485_RX_to_parse;
extern DMA_HandleTypeDef hdma_usart1_rx;
/* USER CODE END 0 */

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
/**/
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;



/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/


/**
 * \brief           Check for new data received with DMA
 */
void usart_RS485_rx_check(void) {
	 new_data_rx=false;
	 if ((buffer_RX_RS232[0]=='#') && (buffer_RX_RS232[5]=='*')){
		 switch(buffer_RX_RS232[1]){
		        case(0):
		        case(1):
		        case(2):
		        case(3):
		        case(4):
		        case(5)://set duty LEDs RGB
		        //case(6):
				         led_to_change=buffer_RX_RS232[1];
						 duty_red=buffer_RX_RS232[2];
						 duty_green=buffer_RX_RS232[3];
						 duty_blue=buffer_RX_RS232[4];
						 new_data_rx=true;
						 parameter_to_read=NOTHING_TO_READ;
						 wait_reply=false;
				         break;
		        case(7)://set char to DISPLAY_16Seg
						 char_rx_to_display=buffer_RX_RS232[2];
						 parameter_to_read=NOTHING_TO_READ;
						 wait_reply=false;
						 new_data_rx=true;
				         break;
		        case(8)://read bitmap tasti
						 parameter_to_read=KEYBOARD_BITMAP_READ;
						 new_data_rx=true;
						 wait_reply=true;
		     		     break;
		        case(9):
		                 //read encoder
						 parameter_to_read=ENCODER_READ;
						 new_data_rx=true;
						 wait_reply=true;
		     		     break;
		        case(10):
		                 //read fw release
						 parameter_to_read=FW_REL_READ;
						 new_data_rx=true;
						 wait_reply=true;
		     		     break;

		        case(20): //read State of OPERA KeyBoard
						 parameter_to_read=OPERA_STATUS_READ;
						 new_data_rx=true;
						 wait_reply=true;
						 break;

		        case(30)://set Configuration Keyboard OPERA
						 if (buffer_RX_RS232[2]<2){
							 opera_keyboard_conf=buffer_RX_RS232[2];
							 if (opera_state==OPERA_NOT_CONFIG){
								 opera_state=OPERA_READY;
							 }
						 }
						 new_data_rx=true;
						 parameter_to_read=NOTHING_TO_READ;
						 wait_reply=false;
						 break;

		        case(40)://set TIMEOUT Long press Keys Function in millisecond
						 longpress_time_ms=((buffer_RX_RS232[2]<<8)+buffer_RX_RS232[3]);
						 new_data_rx=true;
						 parameter_to_read=NOTHING_TO_READ;
						 wait_reply=false;
						 break;

				case(50)://set time interclick for double click  Function in millisecond
						 interclik_time_ms=((buffer_RX_RS232[2]<<8)+buffer_RX_RS232[3]);
						 new_data_rx=true;
						 parameter_to_read=NOTHING_TO_READ;
						 wait_reply=false;
						 break;
		        default:


		        	     break;
		 }
     }
}
/**
 * \brief           Check if DMA is active and if not try to send data
 */
uint8_t usart_RS485_start_dma_transfer(void) {
    uint8_t started = 0;

    /* Check if DMA is active */
    /* Must be set to 0 */


    return started;
}

/**
 * \brief           Process received data over UART
 * \note            Either process them directly or copy to other bigger buffer
 * \param[in]       data: Data to process
 * \param[in]       len: Length in units of bytes
 */
void usart_RS485_process_data(const void* data, size_t len) {
    /* Write data to buffer */
    //ringbuff_write(&usart_tx_dma_ringbuff, data, len);

    /* Start DMA transfer if not already */
    //usart_start_dma_transfer();

unsigned char rs485_cmd=0;
	//pos;
unsigned char byte_rx;

byte_rx=RS485_RX_to_parse.data[RS485_RX_to_parse.last_pos];
switch(RS485_RX_to_parse.state) {

      case (START_BYTE_ADR_STATE):
    		                            if(byte_rx==DP_SW_RS485_adr)
    		                            {
    		                               RS485_RX_to_parse.last_pos=0;
		                                   RS485_RX_to_parse.state=FUNC_CODE_STATE;
		                                   RS485_RX_to_parse.last_pos++;
		                                   RS485_RX_to_parse.frame_check=0;//frame parsing run
		                                   RS485_RX_to_parse.crc_check=byte_rx;//crc_checksum
    		                            }else{
    		                               RS485_RX_to_parse.state=START_BYTE_ADR_STATE;
    		                               RS485_RX_to_parse.last_pos=0;
    		                            }
    		                            break;

      case (FUNC_CODE_STATE):           if((byte_rx>0x0F)||(byte_rx<0x40 ) )
										{
    	                                   //rs485_cmd=RS485_RX_to_parse.data[RS485_RX_to_parse.last_pos];
                                           if ( ((byte_rx==0x20)||(byte_rx==0x21)||(byte_rx==0x23)||(byte_rx==0x24))){//read command
    	                                   RS485_RX_to_parse.state=CRC_LOW_STATE;
										   RS485_RX_to_parse.last_pos++;
										   RS485_RX_to_parse.len_data=0;
										   RS485_RX_to_parse.size=0;
										   RS485_RX_to_parse.command=byte_rx;
										   RS485_RX_to_parse.crc_check+=byte_rx;//crc_checksum
                                           }else if ((rs485_cmd==0x10)||(rs485_cmd==0x11)||(rs485_cmd==0x3C)){//write command single word=2 bytes
                                        	   RS485_RX_to_parse.state=DATA_FIRST_STATE;
											   RS485_RX_to_parse.last_pos++;
											   RS485_RX_to_parse.len_data=2;
											   RS485_RX_to_parse.size=2;
											   RS485_RX_to_parse.command=byte_rx;
											   RS485_RX_to_parse.crc_check+=byte_rx;//crc_checksum
                                           }else if ((byte_rx==0x30)||(byte_rx==0x33)||(byte_rx==0x36)||(byte_rx==0x39)){//write command 3 bytes
                                        	   RS485_RX_to_parse.state=DATA_FIRST_STATE;
											   RS485_RX_to_parse.last_pos++;
											   RS485_RX_to_parse.len_data=3;
											   RS485_RX_to_parse.size=3;
											   RS485_RX_to_parse.command=byte_rx;
											   RS485_RX_to_parse.crc_check+=byte_rx;//crc_checksum
                                           }
										}else{
											 RS485_RX_to_parse.state=START_BYTE_ADR_STATE;
											 RS485_RX_to_parse.last_pos=0;
											 RS485_RX_to_parse.len_data=0;
											 RS485_RX_to_parse.size=0;
									         RS485_RX_to_parse.command=0;
									         RS485_RX_to_parse.frame_check=0;//frame parsing run
									         RS485_RX_to_parse.crc_check=0;//crc_checksum

										}

         		                        break;

      case (DATA_FIRST_STATE):          if (RS485_RX_to_parse.len_data>1){
    	                                   RS485_RX_to_parse.data_body[RS485_RX_to_parse.size-RS485_RX_to_parse.len_data].bytes=byte_rx;
    	                                   RS485_RX_to_parse.len_data--;
    	                                   RS485_RX_to_parse.state=DATA_FIRST_STATE;
										   RS485_RX_to_parse.crc_check+=byte_rx;//crc_checksum
                                        }else{
                                           RS485_RX_to_parse.state=CRC_LOW_STATE;
										   RS485_RX_to_parse.last_pos++;
										   RS485_RX_to_parse.data_body[RS485_RX_to_parse.size- RS485_RX_to_parse.len_data].bytes=byte_rx;
										   RS485_RX_to_parse.len_data--;
										   RS485_RX_to_parse.size=0;
										   RS485_RX_to_parse.crc_check+=byte_rx;//crc_checksum

                                        }

         		                        break;


      case (DATA_SUCC_STATE):
         		                        break;


      case (CRC_LOW_STATE):
    		                            RS485_RX_to_parse.state=CRC_HIGH_STATE;
									    RS485_RX_to_parse.last_pos++;
									    RS485_RX_to_parse.crc_rx=byte_rx;

         		                        break;

      case (CRC_HIGH_STATE):
										RS485_RX_to_parse.crc_rx |=((uint16_t)byte_rx<<8);
									    RS485_RX_to_parse.last_pos=0;
									    RS485_RX_to_parse.state=START_BYTE_ADR_STATE;
									    RS485_RX_to_parse.last_pos=0;
									    if ( RS485_RX_to_parse.crc_check ==RS485_RX_to_parse.crc_rx ){
									       RS485_RX_to_parse.frame_check=1;//frame received is ok
									       RS485_RX_to_parse.crc_check=0;//crc_checksum
									       RS485_RX_to_parse.crc_rx=0;
									    }else{
									       RS485_RX_to_parse.frame_check=0;//
									    }
         		                        break;
      case (FRAME_OK_STATE):
         		                        break;
      case (ERR_STATE):
         		                        break;
      default :
         		                        break;

    }

}

/**
 * \brief           Send string to USART
 * \param[in]       str: String to send
 */
void usart_RS485_send_buffer(void) {
    //usart_RS485_process_data(buffer, len);
	int16_t data_high;
	uint16_t data_low;
	if  (wait_reply==true){
		 RS485_RX_DISABLE;
		 RS485_TX_ENABLE;
		 switch (parameter_to_read){
		         case(NOTHING_TO_READ):
				                            break;
		         case(KEYBOARD_BITMAP_READ):
		        		                    buffer_TX_RS232[0]='#';

		                                    	data_high=keyboard_raw.bitmap;
												data_low =(data_high >>8);
												buffer_TX_RS232[1]=(uint8_t)data_low;
												//data_low=keyboard_raw.bitmap;
												data_low =(data_high & 0x00FF);
												buffer_TX_RS232[2]=(uint8_t)data_low;


		                                    buffer_TX_RS232[3]='*';
		                                    HAL_UART_Transmit(&huart1, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);
		                                    HAL_UART_Transmit(&huart2, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);

		        				            break;
		         case(KEYBOARD_LOGIC_READ):


		        				            break;
		         case(ENCODER_READ):
		                                    buffer_TX_RS232[0]='#';
											if (encoder_data->Rotation==Enc_Rotate_Decrement){
												data_high=(int16_t)(encoder_data->LastDetentRead);
												data_low =(data_high >>8);
												buffer_TX_RS232[1]=(uint8_t)data_low;
												data_low =(data_high & 0x00FF);
												buffer_TX_RS232[2]=(uint8_t)data_low;
												encoder_data->LastDetentRead=0;
											}else if (encoder_data->Rotation==Enc_Rotate_Increment){
												data_high=(int16_t)(encoder_data->LastDetentRead);
												data_low =(data_high >>8);
												buffer_TX_RS232[1]=(uint8_t)data_low;
												data_low =(data_high & 0x00FF);
												buffer_TX_RS232[2]=(uint8_t)data_low;
												encoder_data->LastDetentRead=0;
											}else {
												buffer_TX_RS232[1]=0x00;
												buffer_TX_RS232[2]=0x00;

											}
											buffer_TX_RS232[3]='*';
										    HAL_UART_Transmit(&huart1, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);
										    HAL_UART_Transmit(&huart2, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);
		        				            break;

		         case(FW_REL_READ):
											buffer_TX_RS232[0]='#';
											data_low =(version_fw >>8);
											buffer_TX_RS232[1]=(uint8_t)data_low;
											buffer_TX_RS232[2]=(uint8_t)version_fw;
											buffer_TX_RS232[3]='*';
											HAL_UART_Transmit(&huart1, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);
											HAL_UART_Transmit(&huart2, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);
											break;
		         case(OPERA_STATUS_READ):
											buffer_TX_RS232[0]='#';
											data_low =(version_fw >>8);
											if(opera_state==OPERA_ERROR){
												buffer_TX_RS232[1]=0xFF;
											}else{
											 buffer_TX_RS232[1]=0;
											}
											buffer_TX_RS232[2]=(uint8_t)opera_state;
											buffer_TX_RS232[3]='*';
											HAL_UART_Transmit(&huart1, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);
											HAL_UART_Transmit(&huart2, (uint8_t *)buffer_TX_RS232, 4, 0xFFFF);
											break;
		         default:
		        		                    break;


		 }

		 RS485_TX_DISABLE;
		 RS485_RX_ENABLE;
		 wait_reply=false;
	}

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
void usart_RS485_init(void) {


}
